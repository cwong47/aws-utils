FROM alpine

LABEL name="alpine" \
    version="latest"

RUN apk add --update \
        bash \
        fzf \
        jq \
        py3-pip \
        ruby \
        ruby-json \
        util-linux \
        wget
RUN pip install --upgrade \
        aws-okta-processor \
        awscli

COPY . /opt

RUN for file in /opt/*; do [ -x "$file/${file##*/}" ] && ln -s "$file/${file##*/}" "/usr/local/bin/${file##*/}"; done
