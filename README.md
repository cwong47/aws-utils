# aws-utils

Collections of utlities and tools designed to interact with aws-cli to simplify daily life! ;)

# Motivation

Sick and tired of clicking around AWS console! :P

# Setup

Copy the scripts to either `$HOME/bin` or `/usr/local/bin`, or add this repo to your `$PATH`.

I have something similar in my `.bash_profile`:

```bash
for p in /path/to/aws-utils/*; do
    [ -f ${p}/README.md ] && export PATH=$PATH:$p
done
```

# Docker Usage

All these utilities can be called from the [cwong47/aws-utils](https://hub.docker.com/r/cwong47/aws-utils/) docker image.
I have written a wrapper script to call `docker`. It will be mounting your `$HOME` and `$HOME/.aws` to the container.

```
$ aws-utils ecslookup
default
dev-default-c01
dev-default-gpu-c01
dev-default-nlp-search-c01
...

$ AWS_DEFAULT_REGION=eu-central-1 aws-utils ecslookup
default
prod-euc1-default-c01
staging-euc1-default-c01
```

# Contributors

Calvin Wong

# License

The utilities in this repo are distributed under the
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0),
see LICENSE and NOTICE for more information.
