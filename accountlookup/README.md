# accountlookup

This script simply just displays all the AWS account info.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Command Line Arguments

```
Usage: accountlookup [-h] [-a AccountID] [-t BILLING|OPERATIONS|SECURITY]
       -h                     Print help.
       -a AccountID           AWS account ID to query.
       -t Type                Alternate Contact Type to query.
```

## Fields

Extracting the following fields from organization.

1. Account ID
2. Email
3. Name
4. Status
5. Joined Method
6. Joined Date

Extracting the following fields from alternate contact.

1. Account ID
2. Name
3. Title
4. Phone Number
5. Email
6. Type

## Examples

```
$ accountlookup | column -ts,
123456789012  awsdp-prod@company.com     AWSDP Prod      ACTIVE  INVITED  2021-08-10T15:00:05.213000-07:00
123456789013  devops-prod@company.com    Bigdata Prod    ACTIVE  CREATED  2018-10-08T10:53:24.153000-07:00
123456789014  aws-prod@company.com       AWS Prod        ACTIVE  INVITED  2018-05-09T14:30:23.940000-07:00
123456789015  devops-qa@company.com      DevOps QA       ACTIVE  INVITED  2019-11-04T08:52:10.541000-08:00
123456789016  awsdp-preprod@company.com  AWSDP Non-Prod  ACTIVE  INVITED  2021-08-10T15:52:40.824000-07:00
...

$ accountlookup -a 123456789012 -t SECURITY | column -ts,
123456789012  DevOps  Ops Team  (800) 555-5555  security@company.com  SECURITY
```
