# certlookup

This script returns details about a given certificate.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)
2. [fzf](https://github.com/junegunn/fzf)

## Command Line Arguments

```
Usage: certlookup [us-east-1]
```

## Fields

Certificate List

1. Domain
2. Status
3. InUse
4. RenewalEligibility
5. CertificateArn (ID only)

Preview

1. AdditionalDomains
2. ValidationStatus

## Examples

```
$ certlookup us-east-1
CertificateArn:      arn:aws:acm:us-east-1:012345678900:certificate/15040b8c-****-****-****-d6fd47e08005
DomainName:          www.domain.com
Issuer:              Amazon
CreatedAt:           2025-01-22T23:14:16.627000-08:00
Status:              PENDING_VALIDATION
InUseBy:             []
RenewalEligibility:  INELIGIBLE

DomainName    ValidationStatus    ValidationMethod  ResourceRecordName                ResourceRecordType  ResourceRecordValue
domain01.co   SUCCESS             DNS               _2e8e065***6c3da4a.domain01.co.   CNAME               _07d720***3c1e.crxktfrmng.acm-validations.aws.
domain02.com  PENDING_VALIDATION  DNS               _0ae33dc***5a72010.domain02.com.  CNAME               _7f52f4***a021.zfyfvmchrl.acm-validations.aws.
domain03.com  SUCCESS             DNS               _5b6e67c***053018d.domain03.com.  CNAME               _9c78a0***ba3e.hnyhpvdqhv.acm-validations.aws.
...
```
