# dnsgrep

This script searches thru AWS route53 DNS entries and display them in pretty formats ;)

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [ruby](https://www.ruby-lang.org/en/)

## Command Line Arguments

```
-a                     Query all DNS entries.
-l                     List all available domains.
-d DomainName          Query a specific domain name.
-z <public|private>    Query domain name from DNS zone.
```

## Examples

```
$ dnsgrep
Usage: dnsgrep [-a | -l] [-d DomainName | -z <public|private>]
       -a                     Query all DNS entries.
       -l                     List all available domains.
       -d DomainName          Query a specific domain name.
       -z <public|private>    Query domain name from DNS zone.

$ dnsgrep -l
aws.mycompany.com
aws.mycompany.com
dev.mycompany.com
prod.mycompany.com
mycompany.com
staging.mycompany.com

$ dnsgrep -d aws.mycompany.com
aws.mycompany.com                                            NS         172800     N/A        Public     Z3591XXXZONE01 ns-286.awsdns-35.com.,ns-1007.awsdns-61.net.,ns-1219.awsdns-24.org.,ns-1569.awsdns-04.co.uk.
aws.mycompany.com                                            SOA        900        N/A        Public     Z3591XXXZONE01 ns-286.awsdns-35.com. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400
central-east-bastion-01.aws.mycompany.com                    CNAME      20         N/A        Public     Z3591XXXZONE01 ec2-54-188-45-227.compute-1.amazonaws.com
...
prod-east-search.aws.mycompany.com                           CNAME      60         WRR=100    Public     Z3591XXXZONE02 lb-prod-east-search-1111111111.us-east-1.elb.amazonaws.com
prod-east-search.aws.mycompany.com                           CNAME      60         WRR=0      Public     Z3591XXXZONE02 prod-search-c01-1111111111.us-east-1.elb.amazonaws.com
prod-east-search-exp-10.aws.mycompany.com                    CNAME      300        N/A        Private    Z3591XXXZONE02 ec2-54-47-211-1.compute-1.amazonaws.com
prod-east-search-exp-11.aws.mycompany.com                    CNAME      300        N/A        Private    Z3591XXXZONE02 ec2-54-192-10-112.compute-1.amazonaws.com
prod-east-search-exp-12.aws.mycompany.com                    CNAME      300        N/A        Private    Z3591XXXZONE02 ec2-54-17-180-221.compute-1.amazonaws.com
...


$ dnsgrep -a | grep PATTERN
...
```
