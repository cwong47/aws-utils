#!/usr/bin/env bash
#
#  Author      : Calvin Wong
#  Date        : 20161028
#  What        : Searches DNS entries in AWS Route53.
#  Requirement : awscli, ruby

set -euo pipefail

declare -A opt_domains
declare -A zones=([private]=true [public]=false)

fn_usage() {
    local exit_status="${1:-1}"

    echo "Usage: ${0##*/} [-a | -l] [-d DomainName | -z <public|private>]" 1>&2
    echo "       -a                     Query all DNS entries." 1>&2
    echo "       -l                     List all available domains." 1>&2
    echo "       -d DomainName          Query a specific domain name." 1>&2
    echo "       -z <public|private>    Query domain name from DNS zone." 1>&2
    exit $exit_status
}

[ "$#" -lt 1 ] && fn_usage
while getopts ":d:z:al" opt; do
    case "$opt" in
        d)
            opt_domains["$OPTARG"]=true
            ;;
        z)
            opt_zone="${zones[$OPTARG]}"
            ;;
        a)
            opt_all=true
            ;;
        l)
            opt_list=true
            ;;
        \?)
            echo "Invalid option: -$OPTARG" 1>&2
            fn_usage
            ;;
        :)
            echo "Option -$OPTARG requires an argument ..." 1>&2
            fn_usage
            ;;
    esac
done

aws route53 list-hosted-zones --output json \
    | ruby -rjson -e "j = JSON.parse(ARGF.read);
        j['HostedZones'].sort_by { |i| i['Name'] }.each do |i|;
            printf \"%s %s %s\n\", i['Id'], i['Name'].gsub(/\.$/, ''), i['Config']['PrivateZone'];
        end;" \
            | while read line; do
                set -- $line
                id="${1##*/}"
                name="$2"
                private="$3"

                [ ! "${opt_zone:-}" ] && this_zone="$private" || this_zone="$opt_zone"
                [ "${opt_list:-}" ] && echo "$name" && continue
                if [[ "${opt_domains[$name]:-}" ]] && [[ "$private" == "$this_zone" ]] || [[ "${opt_all:-}" ]]; then
                    [ "$this_zone" == "true" ] && zone_output="Private" || zone_output="Public"
                    aws route53 list-resource-record-sets --output json --hosted-zone-id $id \
                        | ruby -rjson -e "j = JSON.parse(ARGF.read);
                            j['ResourceRecordSets'].each do |i|;
                                printf \"%s %s %s %s \", i['Name'], i['Type'], i['TTL'] || 'N/A', i['Weight'] || 'N/A';
                                o='';
                                if i['ResourceRecords'];
                                    i['ResourceRecords'].each do |a|;
                                        o << a['Value'] << ',';
                                    end;
                                    puts o.gsub(/,$/, '');
                                elsif i['AliasTarget'];
                                    printf \"%s %s\n\", i['AliasTarget']['HostedZoneId'], i['AliasTarget']['DNSName'];
                                end;
                            end;" \
                                | while read entry; do
                                    value="$(echo $entry | awk '{print substr($0, index($0, $5))}')"
                                    set -- $entry
                                    printf "%-60s %-10s %-10s %-10s %-10s %-15s %s\n" "${1%*.}" "$2" "$3" "${4/[0-9]*/WRR=$4}" "$zone_output" "$id" "$value"
                                done
                fi
            done
