# ec2lookup

This script searches thru AWS EC2 entries and display specific fields in pretty CSV formats which can then be parsed ;)

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Config File

Optional RC `run commands` file in `~/.aws-utils/ec2lookup.conf`. See example section for details.

## Command Line Arguments

This script will take any `aws ec2 describe-instances` arguments.

## Fields

Currently extracting the following field from EC2 instances, but can be turned on/off in the config file by setting value to true/false.
1. Instance ID
2. Image ID
3. Availability Zone
4. Instance Type
5. Instance Lifecycle
6. Launch Time
7. Instance State
8. Subnet ID
9. Public IP
10. Private IP
11. Tags - $fields *

\* - User Specific Tags

## Examples

```
$ ec2lookup --region eu-central-1 | head -n 5
i-06975ed7,ami-67b77007,eu-central-1a,m4.large,spot,2016-12-13T19:50:37.000Z,running,subnet-xx34b7af,none,10.11.8.252
i-00d2a42b,ami-67b77007,eu-central-1a,m4.large,spot,2016-12-13T19:49:43.000Z,running,subnet-xx34b7af,none,10.11.8.90
i-0fd83454,ami-67b77007,eu-central-1b,m4.large,spot,2016-12-13T19:50:18.000Z,running,subnet-xx8d5b34,none,10.11.9.7
i-0c8392b5,ami-67b77007,eu-central-1a,m4.large,spot,2016-12-13T19:50:52.000Z,running,subnet-xx34b7af,none,10.11.8.251
i-cc197f71,ami-67b77007,eu-central-1a,m3.large,spot,2016-11-29T00:49:09.000Z,running,subnet-xx63da76,11.111.11.194,10.10.0.161

$ ec2lookup --region eu-central-1 | head -n 5 | column -ts,
i-06975ed7  ami-67b77007  eu-central-1a  m4.large  spot    2016-12-13T19:50:37.000Z  running  subnet-xx34b7af  none           10.11.8.252
i-00d2a42b  ami-67b77007  eu-central-1a  m4.large  spot    2016-12-13T19:49:43.000Z  running  subnet-xx34b7af  none           10.11.8.90
i-0fd83454  ami-67b77007  eu-central-1b  m4.large  spot    2016-12-13T19:50:18.000Z  running  subnet-xx8d5b34  none           10.11.9.7
i-0c8392b5  ami-67b77007  eu-central-1a  m4.large  spot    2016-12-13T19:50:52.000Z  running  subnet-xx34b7af  none           10.11.8.251
i-cc197f71  ami-67b77007  eu-central-1a  m3.large  normal  2016-11-29T00:49:09.000Z  running  subnet-xx63da76  11.111.11.194  10.10.0.161

$ cat ~/.aws-utils/ec2lookup.conf
#  - feel free to change the fields to true/false.
#  - feel free to add additional fields to meet your need.
#
fields=(
    '.InstanceId                                               :true'
    '.ImageId                                                  :true'
    '.Placement.AvailabilityZone                               :true'
    '.InstanceType                                             :true'
    '.InstanceLifecycle // "normal"                            :true'
    '.LaunchTime                                               :true'
    '.State.Name                                               :true'
    '.PublicIpAddress  // "none"                               :true'
    '.PrivateIpAddress // "none"                               :true'
    '(.Tags[]? | select(.Key == "Name").Value) // "none"       :true'
    '(.Tags[]? | select(.Key == "FQDN").Value) // "none"       :true'
    '(.Tags[]? | select(.Key == "Environment").Value) // "none":true'
    '(.Tags[]? | select(.Key == "Service").Value) // "none"    :true'
)
$ ec2lookup --region eu-central-1 | head -n 5
i-06975ed7,ami-67b77007,eu-central-1a,m4.large,normal,2016-12-13T19:50:37.000Z,running,none,10.11.8.252,prod-euc1-tribe-01-c01,prod-euc1-tribe-01-c01.prod.company.com,Production,pitch
i-00d2a42b,ami-67b77007,eu-central-1a,m4.large,normal,2016-12-13T19:49:43.000Z,running,none,10.11.8.90,prod-euc1-elasticsearch-01-c04,prod-euc1-elasticsearch-01-c04.prod.company.com,Production,search
i-0fd83454,ami-67b77007,eu-central-1b,m4.large,normal,2016-12-13T19:50:18.000Z,running,none,10.11.9.7,prod-euc1-elasticsearch-02-c01,prod-euc1-elasticsearch-02-c01.prod.company.com,Production,search
i-0c8392b5,ami-67b77007,eu-central-1a,m4.large,normal,2016-12-13T19:50:52.000Z,running,none,10.11.8.251,prod-euc1-convert-01-c01,prod-euc1-convert-01-c01.prod.company.com,Production,converter
i-cc197f71,ami-67b77007,eu-central-1a,m3.large,normal,2016-11-29T00:49:09.000Z,running,11.111.11.194,10.10.0.161,central-euc1-bastion-01,central-euc1-bastion-01.aws.company.com,Operations,bastion

$ ec2lookup --region eu-central-1 | head -n 5 | awk -F"," '{print $6, $7, $8, $9, $10, $11, $12}' | column -t
running  none           10.11.8.252  prod-euc1-tribe-01-c01          prod-euc1-tribe-01-c01.prod.company.com          Production  pitch
running  none           10.11.8.90   prod-euc1-elasticsearch-01-c04  prod-euc1-elasticsearch-01-c04.prod.company.com  Production  search
running  none           10.11.9.7    prod-euc1-elasticsearch-02-c01  prod-euc1-elasticsearch-02-c01.prod.company.com  Production  search
running  none           10.11.8.251  prod-euc1-convert-01-c01        prod-euc1-convert-01-c01.prod.company.com        Production  converter
running  11.111.11.194  10.10.0.161  central-euc1-bastion-01         central-euc1-bastion-01.aws.company.com          Operations  bastion
```

## Version 001

Started off using sed/tr to parse text output.

```bash
#!/usr/bin/env bash
#
#  https://stackoverflow.com/questions/20706886/aws-cli-how-can-i-query-list-values

profile="default"
regions="${1:-us-east-1}"
query='Reservations[*].Instances[*].[InstanceId,Placement.AvailabilityZone,LaunchTime,State.Name,PrivateIpAddress,PublicIpAddress,InstanceType,ImageId,SubnetId,Architecture,Tags[?Key==`rfk:servertype`].Value[],Tags[?Key==`FQDN`].Value[]]'

for region in $regions; do
    aws ec2 describe-instances \
        --profile $profile \
        --region $region \
        --output text \
        --query $query | \
    tr '\n' '%' | \
    sed -e $'s/%i-/\\\ni-/g' | \
    tr '%' '\t'
done | nl
```

## Version 002

Re-wrote in ruby to parse JSON output.

```ruby
#!/usr/bin/env ruby

require 'rubygems' #otherwise ,require 'json' will fail
require 'json'

input_file = ARGV[0]
File.file?(input_file) || exit(1)
File.zero?(input_file) && exit(2)

topObject = JSON.parse(File.read(input_file));

topObject["Reservations"].each do |reservation|
  reservation["Instances"].each do |instance|
    instance_id         = instance["InstanceId"]                    ||= "None"
    instance_region     = instance["Placement"]["AvailabilityZone"] ||= "None"
    instance_type       = instance["InstanceType"]                  ||= "None"
    instance_state      = instance["State"]["Name"]                 ||= "None"
    instance_pub_ip     = instance["PublicIpAddress"]               ||= "None"
    instance_priv_ip    = instance["PrivateIpAddress"]              ||= "None"
    instance_name       = "None"
    instance_fqdn       = "None"
    instance_env        = "None"
    instance_servertype = "None"

    #  Tags might be nil
    #
    unless instance["Tags"].nil?
      instance["Tags"].each do |key, value|
        case key["Key"]
        when "Name"
          instance_name = key["Value"]
        when "FQDN"
          instance_fqdn = key["Value"]
        when "Environment"
          instance_env = key["Value"]
        when "rfk:servertype"
          instance_servertype = key["Value"]
        end
      end
    end

    printf("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", instance_id, instance_region, instance_type, instance_state, instance_pub_ip, instance_priv_ip, instance_name, instance_fqdn, instance_env, instance_servertype)
  end
end
```

## Version 003

Re-wrote in version 002 to include `aws ec2 describe-instances` as input instead from JSON file.

```diff
6,10c6,9
< input_file = ARGV[0]
< File.file?(input_file) || exit(1)
< File.zero?(input_file) && exit(2)
<
< topObject = JSON.parse(File.read(input_file));
---
> cmd_output = `aws ec2 describe-instances #{ARGV.join(" ")} 2> /dev/null`
> result     = $?.success?
> cmd_output.empty? && exit(1)
> topObject  = JSON.parse(cmd_output)
```
