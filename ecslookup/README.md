# ecslookup

This script makes tons of AWS API calls performing the following tasks:
- List clusters
- List Services of cluster
- Get information of cluster/service
    - cluster info
    - ec2 instances
    - autoscale capacity

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Command Line Arguments

```
Usage: ecslookup [-h] [ClusterName] [ServiceName]
       -h                     Print help.
       ClusterName            Cluster name to query.
       ServiceName            Service name to query.
```

## Examples

```
$ ecslookup dev-default-c01
dev-banners-c01
dev-converter-c01
dev-demo-c01
dev-email-c01
dev-pitch-c01
dev-screenshare-c01

$ ecslookup dev-default-c01 dev-permissions-c01
{
  "cluster": "dev-default-c01",
  "status": "ACTIVE",
  "desired_count": 2,
  "pending_count": 0,
  "running_count": 2,
  "placement_contraints": [],
  "placement_strategy": [],
  "scheduling_stragegy": "REPLICA",
  "task_definition": "dev-permissions-c01:1783",
  "container_definition": [
    {
      "name": "dev-permissions-c01",
      "cpu": 1024,
      "memory": 2048
    }
  ],
  "load_balancing": [
    {
      "container_name": "dev-permissions-c01",
      "containerPort": 80
    }
  ],
  "deployment_options": [
    {
      "min_healthy_percent": 50,
      "max_percent": 200
    }
  ],
  "deployments": [
    {
      "status": "PRIMARY",
      "desired_count": 2,
      "pending_count": 0,
      "running_count": 2
    }
  ],
  "autoscale_capacity": {
    "min_tasks": 2,
    "max_tasks": 2
  },
  "latest_event": "(service dev-permissions-c01) has reached a steady state.",
  "ec2_instances": [
    {
      "instance_id": "i-0f071030xxxxxxxxx",
      "public_ip": null,
      "private_ip": "10.10.9.165"
    },
    {
      "instance_id": "i-0135ec35xxxxxxxxx",
      "public_ip": null,
      "private_ip": "10.10.11.235"
    }
  ]
}
```
