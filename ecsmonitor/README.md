# ecsmonitor

This script shows health status of service(s) in your ECS cluster(s) in the following format:
- heath status
- running and desired count
- task definition

For detailed output, use `ecslookup`.

## Health

The script will list the clusters and fetch details about services that are running in it.
Then it determines if the service is healthy or not:
- `[ OK ]` means that its desiredCount equals its runningCount and it has reached a steady state.
- `[WARN]` means that its desiredCount and its runningCount equal 0 but it still has reached a steady state.
- `[FAIL]` means that it has not reached a steady state.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Command Line Arguments

```
Usage: ecsmonitor [-h] <ClusterName> <ServiceName ...>
       -h                     Print help.
       ClusterName            Cluster name to query.
       ServiceName            Service(s) name to query.
```

## Examples

```
$ ecsmonitor dev-default-c01 dev-{banners,converter,demo,email,pitch,screenshare}-c01
[FAIL] dev-banners-c01                          running 1/2   (dev-banners-c01:3)
[WARN] dev-converter-c01                        running 0/0   (dev-converter-c01:137)
[ OK ] dev-demo-c01                             running 1/1   (dev-demo-c01:1498)
[ OK ] dev-email-c01                            running 1/1   (dev-email-c01:1554)
[ OK ] dev-pitch-c01                            running 1/1   (dev-pitch-c01:1526)
[ OK ] dev-screenshare-c01                      running 1/1   (dev-screenshare-c01:953)
```

## Reference

This was written because [ecs-tools](#https://github.com/flou/ecs-tools) is no longer active.
I use that a lot at work for verifying cluster health during pre/post deployment.
