# ecsrun

This script lets you lookup cluster, service, task to run ECS Exec.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)
3. [fzf](https://github.com/junegunn/fzf)
4. [(optional) session-manager-plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html)

## Command Line Arguments

```
Usage: ecsrun [-h] [-c ClusterName] [-s ServiceName] [-e Command]
       -h                     Print help.
       -c ClusterName         Cluster name to query.
       -s ServiceName         Service name to query.
       -e Command             Command to execute (Default is /bin/sh).
```

## Examples

```
$ ecsrun -c qa-default-c01

  qa-bastion-01
  qa-es-plugin-cerebro-01
  ...
  qa-permissions-01
> qa-permissions-02
  11/11
select qa-default-01 service›
```
