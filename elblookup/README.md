# elblookup

This script lists all the Load Balancers from your AWS account in
the default region. Or you can specify a given ALB/ELB to get the health status.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Command Line Arguments

```
Usage: elblookup [-h | -a | -e] [LoadBalancer]
       -h                     Print help.
       -a                     List all available ALBs.
       -e                     List all available ELBs.
       LoadBalancer           Health status of given Load Balancer.
```

## ALB examples

```
$ elblookup -a | column -ts, | head -n 5
dev-convert-c01     internal-dev-convert-c01.us-east-1.elb.amazonaws.com
dev-crunch-c01      internal-dev-crunch-c01.us-east-1.elb.amazonaws.com
dev-crunch-tsc-c01  dev-crunch-tsc-c01.us-east-1.elb.amazonaws.com
dev-pitch-c01       internal-dev-pitch-c01.us-east-1.elb.amazonaws.com
dev-pitch-tsc-c01   dev-pitch-tsc-c01.us-east-1.elb.amazonaws.com

$ elblookup -a dev-convert-c01
{
  "alb_name": "dev-convert-c01",
  "dns_name": "internal-dev-convert-c01.us-east-1.elb.amazonaws.com",
  "type": "application",
  "scheme": "internet-facing",
  "listeners": [
    {
      "port": 443,
      "protocol": "HTTPS",
      "certificate": [
        {
          "CertificateArn": "arn:aws:acm:us-east-1:xxx:certificate/yyy"
        }
      ]
    },
    {
      "port": 80,
      "protocol": "HTTP",
      "certificate": null
    }
  ],
  "target_groups": [
    {
      "name": "dev-convert-c01",
      "health_check": "/ping",
      "health_check_code": "200"
    },
    {
      "name": "dev-convert-default-c01",
      "health_check": "/",
      "health_check_code": "200"
    }
  ],
  "available_zones": [
    "us-east-1b",
    "us-east-1c",
    "us-east-1e",
    "us-east-1d"
  ]
}
```

## ELB examples

```
$ elblookup -e | column -ts, | head -n 5
lb-prod-east-convert  lb-prod-east-convert.us-east-1.elb.amazonaws.com
lb-prod-east-crunch   lb-prod-east-crunch.us-east-1.elb.amazonaws.com
lb-prod-east-page     lb-prod-east-page.us-east-1.elb.amazonaws.com
lb-prod-east-pitch    lb-prod-east-pitch.us-east-1.elb.amazonaws.com
lb-prod-east-search   lb-prod-east-search.us-east-1.elb.amazonaws.com

$ elblookup -e lb-prod-east-convert
{
  "elb_name": "lb-prod-east-convert",
  "dns_name": "lb-prod-east-convert.us-east-1.elb.amazonaws.com",
  "scheme": "internet-facing",
  "listeners": [
    {
      "in": 80,
      "out": 8080,
      "protocol": "HTTP"
    },
    {
      "in": 6050,
      "out": 6050,
      "protocol": "TCP"
    },
    {
      "in": 443,
      "out": 8080,
      "protocol": "HTTPS",
      "certificate": "arn:aws:acm:us-east-1:xxx:certificate/yyy"
    }
  ],
  "health_check": "TCP:8080",
  "available_zones": [
    "us-east-1b",
    "us-east-1c"
  ],
  "instances": {
    "i-e932xxxx": {
      "prod-east-convert-01": "OutOfService"
    },
    "i-475cxxxx": {
      "prod-east-convert-02": "InService"
    }
  }
}
```
