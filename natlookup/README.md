# natlookup

This script simply just displays all the NAT info.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Config File

Optional RC `run commands` file in `~/.aws-utils/natlookup.conf`. See example section for details.

## Command Line Arguments

This script will take any `aws ec2 describe-nat-gateways` arguments.

## Fields

Currently extracting the following field from NAT gateways, but can be turned on/off in the config file by setting value to true/false.
1. Name
2. NAT Geateway ID
3. Status
4. Elastic IP Address
5. Private IP Address
6. Network Interface
7. VPC
8. Subnet
9. Creation Time

## Examples

```
$ natlookup | head -n 5
central-nat,nat-*****8dc63f173f34,available,52.24.21.196,10.16.0.6,eni-*****350,vpc-*****cdc,subnet-*****029,2016-07-05T16:28:14.000Z
uat-nat,nat-*****fae60d755a4d,available,35.69.13.46,10.19.2.246,eni-*****c93,vpc-*****9c2792776a80,subnet-*****edae5143f8d7,2018-06-26T20:04:57.000Z
dev-nat,nat-*****b128554b64bd,available,34.20.14.177,10.20.1.223,eni-*****c85,vpc-*****d41,subnet-*****e9f,2017-12-13T20:51:34.000Z
staging-nat,nat-*****60511f0caca2,available,35.18.29.146,10.18.1.24,eni-*****908,vpc-*****dd1,subnet-*****f2a,2017-12-14T19:11:59.000Z
prod-nat,nat-*****3406d6e13dcd,available,54.17.154.66,10.17.2.217,eni-*****d5d,vpc-*****e30,subnet-*****d73,2017-08-25T02:23:13.000Z

$ natlookup | head -n 5 | column -ts, | sort
central-nat  nat-*****8dc63f173f34  available  52.24.21.196  10.16.0.6    eni-*****350  vpc-*****cdc           subnet-*****029           2016-07-05T16:28:14.000Z
dev-nat      nat-*****b128554b64bd  available  34.20.14.177  10.20.1.223  eni-*****c85  vpc-*****d41           subnet-*****e9f           2017-12-13T20:51:34.000Z
prod-nat     nat-*****3406d6e13dcd  available  54.17.154.66  10.17.2.217  eni-*****d5d  vpc-*****e30           subnet-*****d73           2017-08-25T02:23:13.000Z
staging-nat  nat-*****60511f0caca2  available  35.18.29.146  10.18.1.24   eni-*****908  vpc-*****dd1           subnet-*****f2a           2017-12-14T19:11:59.000Z
uat-nat      nat-*****fae60d755a4d  available  35.69.13.46   10.19.2.246  eni-*****c93  vpc-*****9c2792776a80  subnet-*****edae5143f8d7  2018-06-26T20:04:57.000Z

$ cat ~/.aws-utils/natlookup.conf
#  - feel free to change the fields to true/false.
#  - feel free to add additional fields to meet your need.
#
fields=(
    '(.Tags[]? | select(.Key == "Name").Value) // "none":true'
    '.NatGatewayId                                      :true'
    '.State                                             :true'
    '.NatGatewayAddresses[].PublicIp                    :true'
    '.NatGatewayAddresses[].PrivateIp                   :true'
    '.NatGatewayAddresses[].NetworkInterfaceId          :true'
    '.VpcId                                             :true'
    '.SubnetId                                          :true'
    '.CreateTime                                        :true'
)
```
