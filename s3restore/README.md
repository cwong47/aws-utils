# s3restore

This script restore S3 bucket/sub-folders from a given timestamp (yyyy-mm-dd or yyyy-mm-ddThh:mm:ss).

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Command Line Arguments

```
Usage: s3restore [ --dry-run | --list|-l | --match|-m object-match-pattern | --bucket|-b bucket-name | --prefix|-p prefix-of-object | --timestamp|-t yyyy-mm-dd ]
       --dry-run              Dry run.
       --list      | -l       List matched objects.
       --match     | -m       Pattern to match/grep when restoring.
       --bucket    | -b       Bucket name (required).
       --prefix    | -p       Prefix of object (required).
       --timestamp | -t       Objects changed since timestamp to restore, yyyy-mm-dd or yyyy-mm-ddThh:mm:ss format (required).
```

## Examples

First, we get all the object versions. Notice the files have a new version on 2024-03-11.

```
$ aws s3api list-object-versions \
    --bucket example-bucket-name \
    --prefix images/7553 \
    --output json \
    --query 'Versions[].[Key, VersionId, LastModified, IsLatest]' \
    | jq -r '.[] | "Key '\''" + .[0] + "'\'',VersionId '\''" + .[1] + "'\'',LastModified " + .[2] + ",IsLatest " + (.[3] | tostring)' \
    | column -ts,
Key 'images/7553/7553~142561.eop'  VersionId '4XXTb7kvoChhARwTsrQIPe9noK7fFXzt'  LastModified 2024-03-11T20:08:03+00:00  IsLatest true
Key 'images/7553/7553~142561.eop'  VersionId 'FnWrRiI8n9r4WePnb15wFA1m556hWW5P'  LastModified 2024-03-10T16:33:06+00:00  IsLatest false
Key 'images/7553/7553~142562.eop'  VersionId '_OmGPMcQpcnb38TpImjICIMs7yHQrTpb'  LastModified 2024-03-11T20:08:03+00:00  IsLatest true
Key 'images/7553/7553~142562.eop'  VersionId '0yTr5cF7YJxhp3IheYgHCfpsHl1c._yj'  LastModified 2024-03-10T16:33:06+00:00  IsLatest false
Key 'images/7553/7553~142563.eop'  VersionId 'lVJlEeAT.p726T6VnkcW1cbgy.6yRZfx'  LastModified 2024-03-11T20:08:04+00:00  IsLatest true
Key 'images/7553/7553~142563.eop'  VersionId 'l1U.L8SuM2nCUMuyA0mthwbsX9sCnAML'  LastModified 2024-03-10T16:33:07+00:00  IsLatest false
...
```

Lets list files that were updated after 2024-03-15. In this case, the results show `IsLatest true` because these files have not been changes since 2024-03-11.

```
$ s3restore --bucket example-bucket-name --prefix images/7553 --timestamp 2024-03-15 --list
Key 'images/7553/7553~142561.eop'  VersionId '4XXTb7kvoChhARwTsrQIPe9noK7fFXzt'  LastModified 2024-03-11T20:08:03+00:00  IsLatest true
Key 'images/7553/7553~142562.eop'  VersionId '_OmGPMcQpcnb38TpImjICIMs7yHQrTpb'  LastModified 2024-03-11T20:08:03+00:00  IsLatest true
Key 'images/7553/7553~142563.eop'  VersionId 'lVJlEeAT.p726T6VnkcW1cbgy.6yRZfx'  LastModified 2024-03-11T20:08:04+00:00  IsLatest true
...
```

Now we list the files that were udpated on 2024-03-11 or later, we know from earlier that they were indeed changed, so the results show `IsLatest false`.

```
$ s3restore --bucket example-bucket-name --prefix images/7553 --timestamp 2024-03-11 --list
Key 'images/7553/7553~142561.eop'  VersionId 'FnWrRiI8n9r4WePnb15wFA1m556hWW5P'  LastModified 2024-03-10T16:33:06+00:00  IsLatest false
Key 'images/7553/7553~142562.eop'  VersionId '0yTr5cF7YJxhp3IheYgHCfpsHl1c._yj'  LastModified 2024-03-10T16:33:06+00:00  IsLatest false
Key 'images/7553/7553~142563.eop'  VersionId 'l1U.L8SuM2nCUMuyA0mthwbsX9sCnAML'  LastModified 2024-03-10T16:33:07+00:00  IsLatest false
...
```

If we want to restore all the files that was updated on 2024-03-11 or later, we simply pass the `--timestamp` as 2024-03-11.

```
$ s3restore --bucket example-bucket-name --prefix images/7553 --timestamp 2024-03-11 --dry-run
aws s3api copy-object --bucket example-bucket-name --copy-source example-bucket-name/images/7553/7553~142561.eop?versionId=FnWrRiI8n9r4WePnb15wFA1m556hWW5P --key images/7553/7553~142561.eop
aws s3api copy-object --bucket example-bucket-name --copy-source example-bucket-name/images/7553/7553~142562.eop?versionId=0yTr5cF7YJxhp3IheYgHCfpsHl1c._yj --key images/7553/7553~142562.eop
aws s3api copy-object --bucket example-bucket-name --copy-source example-bucket-name/images/7553/7553~142563.eop?versionId=l1U.L8SuM2nCUMuyA0mthwbsX9sCnAML --key images/7553/7553~142563.eop
```
