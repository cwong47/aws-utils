# userlookup

This script simply just get all the AWS users.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Command Line Arguments

This script will take any `aws iam list-users` arguments.

## Fields

Currently extracting the following field from IAM users.
1. UserName
2. UserId
3. Path
4. CreateDate
5. PasswordLastUsed
6. Arn

## Examples

```
$ userlookup | head -n 5
alan.gupta,AIDAIEKB6MP**********,/,2017-05-09T23:07:31Z,2019-08-29T02:39:32Z,arn:aws:iam::6079875*****:user/alan.gupta
brian.munoz,AIDAJ2E4ZK5**********,/,2018-12-11T17:33:21Z,2019-08-29T13:28:07Z,arn:aws:iam::6079875*****:user/brian.munoz
calvin.persson,AIDAI6YSWN5**********,/,2016-06-23T19:27:15Z,2019-08-29T12:14:07Z,arn:aws:iam::6079875*****:user/calvin.persson
devon.rago,AIDAJKHP22B**********,/,2017-07-07T17:48:44Z,2019-08-28T12:41:48Z,arn:aws:iam::6079875*****:user/devon.rago
ellen.smith,AIDAJH7M67Y**********,/,2015-05-20T20:10:03Z,2019-08-23T21:32:05Z,arn:aws:iam::6079875*****:user/ellen.smith

$ userlookup | head -n 5 | column -ts,
alan.gupta      AIDAIEKB6MP**********  /  2017-05-09T23:07:31Z  2019-08-29T02:39:32Z  arn:aws:iam::6079875*****:user/alan.gupta
brian.munoz     AIDAJ2E4ZK5**********  /  2018-12-11T17:33:21Z  2019-08-29T13:28:07Z  arn:aws:iam::6079875*****:user/brian.munoz
calvin.persson  AIDAI6YSWN5**********  /  2016-06-23T19:27:15Z  2019-08-29T12:14:07Z  arn:aws:iam::6079875*****:user/calvin.persson
devon.rago      AIDAJKHP22B**********  /  2017-07-07T17:48:44Z  2019-08-28T12:41:48Z  arn:aws:iam::6079875*****:user/devon.rago
ellen.smith     AIDAJH7M67Y**********  /  2015-05-20T20:10:03Z  2019-08-23T21:32:05Z  arn:aws:iam::6079875*****:user/ellen.smith
```
