# natlookup

This script queries some Client VPN Endpoint and related info.

## Requirements

To run this script, you will need the following packages installed.
1. [awscli](https://aws.amazon.com/cli/)
2. [jq](https://stedolan.github.io/jq/)

## Config File

Optional RC `run commands` file in `~/.aws-utils/vpnlookup.conf`. See example section for details.

## Connections Fields

Currently extracting the following field from Client VPN Endpoint Connections, but can be turned on/off in the config file by setting value to true/false.
1. ConnectionId
2. ClientVpnEndpointId
3. Timestamp
4. ConnectionEstablishedTime
5. IngressBytes
6. EgressBytes
7. IngressPackets
8. EgressPackets
9. ClientIp
10. CommonName
11. Status
12. ConnectionEndTime

## Examples

```
$ vpnlookup conn
cvpn-connection-*****,cvpn-endpoint-*****,2020-08-12T18:52:44,2020-08-12T18:36:54,1229381,2774710,6192,5275,10.22.0.130,alan.anderson.aws-vpn.company.com,terminated,2020-08-12T18:52:44
cvpn-connection-*****,cvpn-endpoint-*****,2020-08-12T19:13:38,2020-08-12T18:57:37,884113,6944664,7422,8963,10.22.0.130,barry.bonds.aws-vpn.company.com,active,-

$ vpnlookup cert | column -ts,
client.aws-vpn.company.com         arn:aws:acm:us-east-2:*****:certificate/*****-6d38-4b10-8d1c-*****
server.aws-vpn.company.com         arn:aws:acm:us-east-2:*****:certificate/*****-c706-4dc5-aa86-*****
alan.anderson.aws-vpn.company.com  arn:aws:acm:us-east-2:*****:certificate/*****-615e-4306-916b-*****
barry.bonds.aws-vpn.company.com    arn:aws:acm:us-east-2:*****:certificate/*****-adb3-40dc-a93d-*****
calvin.cook.aws-vpn.company.com    arn:aws:acm:us-east-2:*****:certificate/*****-6aa5-4714-aaf2-*****

$ cat ~/.aws-utils/vpnlookup.conf
#  - feel free to change the fields to true/false.
#  - feel free to add additional fields to meet your need.
#
fields=(
    '.ConnectionId                               :true'
    '.ClientVpnEndpointId                        :true'
    '(.Timestamp | sub(" "; "T"))                :true'
    '(.ConnectionEstablishedTime | sub(" "; "T")):true'
    '.IngressBytes                               :true'
    '.EgressBytes                                :true'
    '.IngressPackets                             :true'
    '.EgressPackets                              :true'
    '.ClientIp                                   :true'
    '.CommonName                                 :true'
    '.Status.Code                                :true'
    '(.ConnectionEndTime | sub(" "; "T"))        :true'
)

#  - required
cvpn_endpoint_id="cvpn-endpoint-***"
cvpn_cert_domain_name="aws-vpn.company.com"
```
